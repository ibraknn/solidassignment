package kz.aitu.oop.practice.practice3;

public class PHP extends Worker implements BackendDeveloper{

    public PHP(String salary, String name) {
        super(salary,name);
    }

    @Override
    public void code() {
        writeBack();
    }

    @Override
    public void work() {
        code();
    }
    @Override
    public void writeBack() {
        System.out.println("My name is "+getName()+". PHP is better than Java for backend!"+" My salary: "+getSalary());
    }
}
