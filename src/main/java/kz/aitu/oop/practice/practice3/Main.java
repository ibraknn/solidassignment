package kz.aitu.oop.practice.practice3;

public class Main {

    public static void main(String[] args) {
        Company company = new Company();
        company.addEmployees(new FullStack("270K$","Ibragim"));
        company.addEmployees(new FullStack("250K$","Abay"));
        company.addEmployees(new PHP("223K$","Azizbek"));
        company.addEmployees(new JavaScript("200K$","Temirlan"));

        //company.deleteEmployees();

        company.startWork();
    }
}
