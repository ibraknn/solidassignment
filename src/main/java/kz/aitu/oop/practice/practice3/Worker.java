package kz.aitu.oop.practice.practice3;

public abstract class Worker extends Salary implements Employee {
    private String name;

    public Worker(String Salary, String name){
        super(Salary);
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
