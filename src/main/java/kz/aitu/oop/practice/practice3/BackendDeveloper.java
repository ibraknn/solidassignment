package kz.aitu.oop.practice.practice3;

public interface BackendDeveloper extends Developer {
    void writeBack();
}
