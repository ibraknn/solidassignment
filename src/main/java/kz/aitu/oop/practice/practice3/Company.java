package kz.aitu.oop.practice.practice3;


import java.util.LinkedList;
import java.util.List;

public class Company {
    private List<Employee> employees = new LinkedList<>();

    public void addEmployees(Employee employee) {
        employees.add(employee);
    }
    public void startWork(){
        employees.forEach(e -> e.work());
    }

    public void deleteEmployees(){employees.clear();}
}
