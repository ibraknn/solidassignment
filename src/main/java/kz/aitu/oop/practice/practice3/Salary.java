package kz.aitu.oop.practice.practice3;

public abstract class Salary implements Employee{
    private String salary;

    public Salary(String salary){
        setSalary(salary);
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

}
