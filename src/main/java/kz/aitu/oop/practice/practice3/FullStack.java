package kz.aitu.oop.practice.practice3;

public class FullStack extends Worker implements FrontendDeveloper, BackendDeveloper {

    public FullStack(String salary, String name) {

        super(salary,name);
    }



    @Override
    public void code() {
        System.out.print("My name is " + getName() + ". I am fullstack developer,");
       writeBack();
       writeFront();
    }

    @Override
    public void work() {
        code();
    }

    @Override
    public void writeBack() {
        System.out.print(" I am using Java ");
    }

    @Override
    public void writeFront() {
        System.out.println("and JavaScript for websites." +" My salary: "+getSalary());
    }
}
