package kz.aitu.oop.practice.practice3;

public interface FrontendDeveloper extends Developer {
    void writeFront();
}
