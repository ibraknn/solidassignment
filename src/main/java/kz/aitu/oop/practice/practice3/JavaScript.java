package kz.aitu.oop.practice.practice3;

public class JavaScript extends Worker implements FrontendDeveloper {

    public JavaScript(String salary, String name) {
        super(salary,name);
    }

    @Override
    public void code() {
        writeFront();
    }

    @Override
    public void work() {
        code();
    }

    @Override
    public void writeFront() {
        System.out.println("My name is "+getName()+". I am developing frontend with JS."+" My salary: "+getSalary());
    }
}
